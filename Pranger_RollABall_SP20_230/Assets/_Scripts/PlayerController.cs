﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    private int count;
    public Text countText;
    public Text winText;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // Score for Text
        count = 0;
        //Updates countText
        SetCountText();
        //Defaul value for winText
        winText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Rolls the Ball
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Destroys Cube and Increases Count
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        //Sets the Score Text to How Many Cubes Have Been Collected
        countText.text = "Count: " + count.ToString();

        //Checks to see if all cubes have been collected
        if (count >= 12)
        {
            winText.text = "You win! The game will now reset!";
            //Waits for certain amount of time to reset the scene
            StartCoroutine(RestartScene());
        }
    }
    IEnumerator RestartScene()
    {
        //Timer Code
        yield return new WaitForSeconds(3);
        //Runs this code AFTER the timer runs out
        SceneManager.LoadScene("Play Area");
    }
}
