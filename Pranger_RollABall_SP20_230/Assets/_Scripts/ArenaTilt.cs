﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaTilt : MonoBehaviour


{
    public GameObject arena;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");


        //Keeps "untilting" the Y Axis
        if (arena.transform.rotation.y != 0)
        {
            transform.Rotate(new Vector3(0f, -(arena.transform.rotation.y) * 10, 0f));
        }

        //Won't tilt arena right past a certain point
        if (arena.transform.rotation.z > -0.25 & moveHorizontal > 0)
        {
            transform.Rotate((new Vector3(moveVertical, 0f, (-1 * moveHorizontal))));
        }

        //Won't tilt arena left past a certain point
        if (arena.transform.rotation.z < 0.25 & moveHorizontal < 0)
        {
            transform.Rotate((new Vector3(moveVertical, 0f, (-1 * moveHorizontal))));
        }

        //Won't tilt arena down past a certain point
        if (arena.transform.rotation.x > -0.25 & moveVertical < 0)
        {
            transform.Rotate((new Vector3(moveVertical, 0f, (-1 * moveHorizontal))));
        }

        //Won't tilt arena up past a certain point
        if (arena.transform.rotation.x < 0.25 & moveVertical > 0)
        {
            transform.Rotate((new Vector3(moveVertical, 0f, (-1 * moveHorizontal))));
        }

    }
}
